# Date & Time Calculator

This is a project template for a simple calculator with three date/time-related features:

- calculating the number of days between two dates
- calculating the date some multiplied interval after a start date
- calculating all recurring event dates and times between two dates

The UI code is already set up; you will be implementing all of the algorithmic code for the calculator.

## Project goals

In practice, real-world software development is mostly about identifying and combining pre-existing solutions. It is extremely uncommon for an organization to create an entire codebase from scratch without using any third-party code.

This makes our job easier in some ways, but it also makes our job harder in some ways! There is **too much** third-party code for any one person to know about all of it, and more is being created every day. Nearly every library that you can learn today will be outdated in ten years.

A good developer is good at **learning** new third-party tools and figuring out how to apply them to solve problems. This is why we read documentation, and it's one of the most common reasons that we read code: to understand and evaluate a third-party solution that we might want to apply to our problem.

In this project, you will use two third-party tools that are probably unfamiliar to you, and you will be expected to figure out how to use them from examples and documentation. This experience will be especially useful later in the course when we discuss the craft of writing good documentation!

## Library information

For time calculations, we will be working with the `js-joda` library. Use the [API Reference](https://js-joda.github.io/js-joda/identifiers.html#core-src) to figure out how to work with it.

We will also be using TypeScript arrays, which you can find documentation for on the [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array).

For testing, we will be working with the `jest` library. Use the [API Reference](https://jestjs.io/docs/api) to figure out how to work with it.

## Requirements

You must edit only the `src/Calculator.ts` and `src/Calculator.test.ts` files. (Imagine the other files are owned by a different team in your organization.)

Your code must compile with no errors **or warnings** in the files that you've edited. This includes ESLint warnings.

You must write tests for each function in `src/Calculator.test.ts`. At minimum, you must write one test for each provided example in the assignment instructions.

## Grading

Your code will be graded on whether it correctly implements the specified algorithms and meets the above requirements. You will lose points for ESLint warnings and errors.

## Setup

You should be able to access the codebase for this assignment at https://gitlab.cecs.pdx.edu/cas28/datetime-calculator, using the LDAP login with the same credentials that you use to sign onto the CS department Linux servers (your MCECS credentials). Do not download this project directly: instead, click the "fork" button near the upper-right corner of this page to create your own fork (a copy of the codebase that you are the owner of). **Do not change the name of your fork:** it should be called `datetime-calculator` like the original.

You should be taken to your new fork. Take note of the URL. Click "Settings" in the GitLab menu and expand the "Visibility, project features, permissions" section, and then set the "Project visibility" dropdown to "Internal" and set the "Issues", "Repository", "Merge requests", and "Forks" dropdowns to "Only project members". Then, click "Members" in the GitLab "Project Information" submenu and add me (@cas28) with a "Guest" role permission. Make sure to click the "Save" button after changing the settings.

Finally, follow [these instructions from GitLab](https://docs.gitlab.com/ee/user/ssh.html) to set up an SSH key on your GitLab account, which will be the "password" that VSCode automatically uses when communicating with GitLab.

## Submitting

To submit your work on the project, push your code to your `datetime-calculator` fork on GitLab.

To check whether I can see your fork, you can use this web tool: <https://web.cecs.pdx.edu/~cas28/can-katie-see-my-repo?username=YOUR_USERNAME_HERE>, replacing YOUR_USERNAME_HERE with your GitLab username. For example, if your GitLab username was `abc123`, you could go to <https://web.cecs.pdx.edu/~cas28/can-katie-see-my-repo?username=abc123> to find out if I can see your fork.

If the web tool says that I can see your repo, then I can see all of the code that you've pushed to your fork. This does not guarantee that you have actually pushed all of your work, so you should double-check that yourself.

If the web tool says that I cannot see your repo, **I am incapable of grading your work**, because I do not have permission to see your work. Re-read the setup instructions above and follow every step carefully. If you follow the setup instructions correctly, the web tool will be able to see your repo.

## Calculator input formats

All inputs to the calculator are in ISO standard notation.

All of the dates and times we work with will be "timezone-less": this means we're ignoring details like Daylight Saving Time. To be precise, we're working with [UTC](https://en.wikipedia.org/wiki/Coordinated_Universal_Time), which is uncommon for humans to use but very common for computers to use.

- Dates (e.g. `1991-12-31`): separated by `-`, four-digit year, two-digit month, two-digit day
- Times (e.g. `20:59`): separated by `:`, 24-hour clock with hours and minutes
- Date/times (e.g. `1991-12-31T20:59`): separated by `T`, a date followed by a time
- Periods (e.g. `P1Y2M30D`): **starts** with a `P`, number of years followed by `Y`, number of months followed by `M`, number of days followed by `D`; each part except the `P` is optional

You will only need to think about these formats when **testing** your calculator: the library we use to handle date calculations will automatically handle converting date/time/period values to and from strings.

You will not need to handle invalid input; the UI code is already set up to handle it for you. You can assume all of the arguments passed to your functions are valid.

## Calculator features

Here are the specifications for each calculator feature you need to implement.

### `daysBetween(start, end)`

The arguments `start` and `end` are both dates (not date/times). The return value should be the (integer) number of days between `start` and `end`. The range calculation should be **exclusive**: if `start` and `end` are the same day then the correct result is **0**, not 1.

If `start` is a date that comes after `end`, then the result should be negative.

Examples:

```
start = 2022-01-31
end = 2022-01-31
return value = 0

start = 2022-01-30
end = 2022-01-31
return value = 1

start = 2022-01-31
end = 2022-01-30
return value = -1

start = 2022-01-31
end = 2022-03-31
return value = 59
```

### `afterIntervalTimes(start, interval, multiplier)`

The `start` argument is a date (not a date/time), the `interval` argument is a period (an amount of days), and the `multiplier` argument is a non-negative integer (this is guaranteed by the UI, it won't ever be negative or fractional). The return value should be the date obtained by starting at `start` and adding the `interval` period `multiplier` times.

Adding months to a date is somewhat strange: for example, what should one month after January 31st 2022 be? The answer we'll go with is February 28th (since it's a non-leap year).

Specifically, adding `x` months should always increment the month counter by **exactly** `x`, and should round down the day counter if necessary to avoid incrementing the month counter by more than `x`.

When adding complex periods that involve multiple units (such as `P1Y1M`, "one year and one month"), we will handle the units in order of **largest to smallest**: so adding `P1Y1M` to a date is equivalent to adding `P1Y` and then adding `P1M`, not the other way around. Again, this is the default behavior of our library, so you won't have to implement this behavior yourself.

Examples:

```
start = 2022-01-31
interval = P1D
multiplier = 3
return value = 2022-02-03

start = 2022-01-31
interval = P1D
multiplier = 0
return value = 2022-01-31

start = 2022-01-31
interval = P1M
multiplier = 1
return value = 2022-02-28

start = 2019-01-31
interval = P1Y1M
multiplier = 1
return value = 2020-02-29
```

### `recurringEvent(start, end, interval, timeOfDay)`

The `start` and `end` arguments are date/times (not just dates), the `interval` argument is a period (an amount of days), and the `timeOfDay` argument is a time (not a date/time).

The return value should be an _array_ of all date/times on which the recurring event should take place: the event will take place once per `interval` at `timeOfDay`. If the `start` time is **earlier in the day** than `timeOfDay`, the first event should take place on the `start` date; otherwise, the first event should take place **exactly** one `interval` after the `start` date. Similarly, if the `end` time is **later in the day** than `timeOfDay` and the last `interval` ends exactly on the `end` date, the last event should take place on the `end` date; otherwise, the last event should take place **at most** one `interval` before the `end` date. In all cases, the time component of every element in the output array should be equal to `timeOfDay`.

This is a complex specification, but if you read it carefully you'll find it describes a fairly intuitive way for a function like this to behave. Make sure to test your code thoroughly!

Examples:

```
start = 2022-01-01T00:00
end = 2022-01-04T23:59
interval = P1D
timeOfDay = 01:00
return value = [
  2022-01-01T01:00,
  2022-01-02T01:00,
  2022-01-03T01:00,
  2022-01-04T01:00,
]

start = 2022-01-01T02:00
end = 2022-01-04T23:59
interval = P1D
timeOfDay = 01:00
return value = [
  2022-01-02T01:00,
  2022-01-03T01:00,
  2022-01-04T01:00,
]

start = 2022-01-01T00:00
end = 2022-01-04T00:00
interval = P1D
timeOfDay = 01:00
return value = [
  2022-01-01T01:00,
  2022-01-02T01:00,
  2022-01-03T01:00,
]

start = 2022-01-31T00:00
end = 2022-05-15T00:00
interval = P1M
timeOfDay = 01:00
return value = [
  2022-01-31T01:00,
  2022-02-28T01:00,
  2022-03-28T01:00,
  2022-04-28T01:00,
]
```
