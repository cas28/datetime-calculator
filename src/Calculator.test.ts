import "jest-extended";

import {
    daysBetween,
    afterIntervalTimes,
    recurringEvent,
} from "./Calculator";

import {
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

describe("daysBetween", () => {
    test("first provided example", () => {
        const start: LocalDate = LocalDate.of(2022, 1, 31);
        const end: LocalDate = LocalDate.of(2022,1,31);
        expect(daysBetween(start, end)).toEqual(0);
    })
})

describe("afterIntervalTimes", () => {

});

describe("recurringEvent", () => {

});